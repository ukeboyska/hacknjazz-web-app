/*global define*/
/*global jQuery*/
define(['jquery','underscore','isonscreen'], function($,_) {
	'use strict';

	var util = {};

	util.scrollTo = function($el) {
        var thisTarget = $el.attr('data-target');
		$('html, body').animate({
	        scrollTop: $( '#'+thisTarget ).offset().top - 50
	    }, 300);
	};
	util.closeModals = function() {
		$('body').removeClass('overhidden');
		util.removeHash();
	};
	util.removeHash = function() {
	    history.pushState('', document.title, window.location.pathname + window.location.search);
	};
	//adjusts height of all columns in footer to be the same
	util.heightify = function($el) {

		//use this in modules you want to have this functionality
		//util.heightify(this.$el);

		var heightifyElem = $el;

		var heightifyThis = function() {

			var totalImages = heightifyElem.find('.grid__cell img').length;
			var imagesLoaded = 0;

			var maxHeight = -1;
            var maxHeightPadless = -1;

			heightifyElem.children('.grid__cell:first,ul:first').each(function() {

				maxHeight = maxHeight > heightifyElem.outerHeight(true) ? maxHeight : heightifyElem.outerHeight(true);
				
				//after all images are loaded, just in case.
				heightifyElem.find('.grid__cell img').each(function() {
					var fakeSrc = heightifyElem.attr('src');
					$('<img/>').attr('src', fakeSrc).css('display', 'none').load(function() {
						imagesLoaded++;
						if (imagesLoaded >= totalImages) {
							maxHeight = maxHeight > heightifyElem.outerHeight(true) ? maxHeight : heightifyElem.outerHeight(true);
						}
					});
				});
			});

			heightifyElem.children('.grid__cell').height(maxHeight);

			var heightifyElemContent = heightifyElem.children('.grid__cell').find('.content');

			if (heightifyElemContent.length > 0) {
				
				heightifyElemContent.each(function() {
					maxHeightPadless = maxHeightPadless > $(this).height() ? maxHeightPadless : $(this).height();
				});

				heightifyElemContent.height(maxHeightPadless);
				
				//after all images are loaded, just in case.
				heightifyElem.find('img').each(function() {
					var fakeSrc = $(this).attr('src');
					$('<img/>').attr('src', fakeSrc).css('display', 'none').load(function() {
						imagesLoaded++;
						if (imagesLoaded >= totalImages) {
							heightifyElemContent.each(function() {
								maxHeightPadless = maxHeightPadless > $(this).height() ? maxHeightPadless : $(this).height();
							});

							heightifyElemContent.height(maxHeightPadless);
						}
					});

				});

			}

		};

		//go initially
		heightifyThis();

		//redo if window is resized
		var debounceResize = _.debounce(function(){
			
			heightifyElem.children('.grid__cell').height('auto');
			if (heightifyElem.find('.content').length > 0) {
				heightifyElem.children('.grid__cell').find('.content').height('auto');
			}
			heightifyThis();

		}, 1000);

		$(window).on('resize',debounceResize);

	};

	//allows class based CSS animation to happen anywhere
	util.screengo = function($el) {

		//use this in modules you want to have this functionality
		//util.screengo(this.$el);

		var screenGoElem = $el;
			
		var goGo = function() {

			//use the go class for CSS animations!
			if(screenGoElem.isOnScreen()) {
				screenGoElem.addClass('go');
			} else {
				screenGoElem.removeClass('go');
			}

		};
		
		//go initially
		var initialize = _.delay(function(){

			goGo();

		}, 200);
		$(window).load(initialize);

		//go if scrolls into view, throttled
		var throttleScroll = _.throttle(function(){

			goGo();

		}, 800);
		$(window).scroll(throttleScroll);

	};

	//allows class based CSS animation to happen anywhere
	util.modalize = function($el) {

		var modalLink = $el;

        modalLink.modalization();

        //for dynamic video modals -- reads special attributes put on link to create this stuff.
        if ($('a[data-modalType*=video]').length > 0) {
            $('a[data-modalType*=video]').each(function () {
                var myModalID = $(this).attr('id');
                var myVideoID = $(this).attr('data-videoID');

                $('body').append('<aside data-piece="modal" class="video" id="'+myModalID+'-popup"><h2 class="screenreader">Video Modal Popup</h2><div class="modal__content"><a href="#" class="modal__close"></a><iframe src="//player.vimeo.com/video/'+myVideoID+'?api=1&amp;portrait=0&amp;color=333&amp;player_id='+myModalID+'" width="640" height="390" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="wrap"></div></div></aside>');

            });
        }
        if ($('a[data-modalType*=image]').length > 0) {
            $('a[data-modalType*=image]').each(function () {
                var myModalID = $(this).attr('id');
                var myImageURL = $(this).find('img').attr('src');

                $('body').append('<aside data-piece="modal" class="image" id="'+myModalID+'-popup"><h2 class="screenreader">Image Modal Popup</h2><div class="modal__content"><a href="#" class="modal__close"></a><img src="'+myImageURL+'"><div class="wrap"></div></div></aside>');

            });
        }

        var lastPos = 0;

        $('[data-piece="modal"],.modal__close').on('click touchstart', function(event){
        	event.preventDefault();
        	$(this).parents('[data-piece="modal"]').removeClass('modal__revealed');
        	//clear hash
    		lastPos = $(window).scrollTop();
        	util.removeHash();
        });
		$('[data-piece="modal"] .modal__content').on('click touchstart', function(event){
			event.stopPropagation();
		});
		//clear hash
        $(window).bind('hashchange',function(){
		    var hash = location.hash.replace('#','');
		    if(hash === '') {
		    	$(window).scrollTop(lastPos);
		    }
		});

	};

    // modal plugin
    (function ($) {
        $.fn.extend({
            modalization: function () {
                
                function closeModal(myModalID) {

                    $('.superz').removeClass('superz');
                    $('[data-piece="modal"]').removeClass('modal__revealed');
        			$('body').removeClass('overhidden');

                    var vimeoWrap = $('#'+myModalID+'-popup');
                    vimeoWrap.html( vimeoWrap.html() );

                }

                return this.each(function () {

                    $(this).on('click',function (e) {
    					
    					$(this).siblings('[data-piece="modal"]').addClass('modal__revealed');

                        e.preventDefault();

                        $(this).parents('.page-width').addClass('superz');
                        $(this).parents('body').addClass('superz');
                        var myModalID = $(this).attr('id');

                        $('[data-piece="modal"]').on('click touchend', function () {
                            closeModal(myModalID);
                        });
                        $('[data-piece="modal"]__close').on('click touchend', function (event) {
                            event.preventDefault();
                            closeModal(myModalID);
        					$('body').removeClass('overhidden');
                        });

                        // console.log($('#'+myModalID));

                        $('#'+myModalID+'-popup').addClass('modal__revealed');
        				$('body').addClass('overhidden');
                        e.preventDefault();
                    });
                });
            }
        });
    })(jQuery);

	return util;

});