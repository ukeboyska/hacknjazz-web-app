/*global require*/
(function() {
    'use strict';
    require.config({
        paths: {
            jquery: '../../bower_components/jquery/dist/jquery',
            Swiper: '../../bower_components/swiper/dist/js/swiper',
            zugzug: '../../bower_components/zugzug/zugzug',
            underscore: '../../bower_components/underscore/underscore-min',
            headroom: '../../bower_components/headroom.js/dist/headroom',
            isonscreen: 'vendor/isonscreen',
            jgestures: 'vendor/jgestures',
            util: 'util'
        },
        shim: {
            'isonscreen': {
                deps: ['jquery']
            },
            'headroom': {
                exports: 'Headroom'
            },
            'underscore': {
                exports: '_'
            }
        }
    });
}());