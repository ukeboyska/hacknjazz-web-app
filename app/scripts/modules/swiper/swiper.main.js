/*global define*/
define(['jquery','underscore','Swiper','util'], function($,_,Swiper,util) {

	'use strict';

	return {
		init: function() {
            
            //the infinite slider

            var swiperContainer = this.$el;

            swiperContainer.removeClass('initializing').addClass('initialized');

            var infiniteSwiper;

            if( swiperContainer.attr('data-swipetype') === 'auto-play' ) {

                //autoplaying swiper
                infiniteSwiper = new Swiper(swiperContainer[0],{
                    autoplay: 4000,
                    speed: 500,
                    calculateHeight: true,
                    //grabCursor: true,
                    loop: true,
                    slidesPerView: 1,
                    loopAdditionalSlides: 1,
                    loopedSlides: 3,
                    centeredSlides: true
                });

            } else {

                //normal one that doesn't autoplay
                infiniteSwiper = new Swiper(swiperContainer[0],{
                    speed: 5000,
                    calculateHeight: true,
                    //grabCursor: true,
                    loop: true,
                    slidesPerView: 1,
                    loopAdditionalSlides: 1,
                    loopedSlides: 3,
                    centeredSlides: true,
                    onSlideChangeStart: function(){
                        $('[data-atom="swiper--arrow"]').addClass('transitioning');
                    },
                    onSlideChangeEnd: function(){
                        $('[data-atom="swiper--arrow"]').removeClass('transitioning');
                        swiperContainer.find('.swiper-slide-active').prev().addClass('prev').siblings().removeClass('prev');
                        swiperContainer.find('.swiper-slide-active').next().addClass('next').siblings().removeClass('next');
                    },
                    onTouchEnd: function(){
                        swiperContainer.find('.swiper-slide-active').prev().addClass('prev').siblings().removeClass('prev');
                        swiperContainer.find('.swiper-slide-active').next().addClass('next').siblings().removeClass('next');
                    }
                });

            }
            swiperContainer.find('.click-left').on('touchstart click',function(e){
                e.preventDefault();
                if(!$('[data-atom="swiper--arrow"]').hasClass('transitioning')) {
                    infiniteSwiper.swipePrev();
                }
            });
            swiperContainer.find('.click-right').on('touchstart click',function(e){
                e.preventDefault();
                if(!$('[data-atom="swiper--arrow"]').hasClass('transitioning')) {
                    infiniteSwiper.swipeNext();
                }
            });
            swiperContainer.find('.swiper-slide-active').prev().addClass('prev').siblings().removeClass('prev');
            swiperContainer.find('.swiper-slide-active').next().addClass('next').siblings().removeClass('next');
        
            infiniteSwiper.update();

            var throttleResize = _.throttle(function(){

                infiniteSwiper.update();

            }, 800);

            $(window).on('resize',throttleResize);

            $.each($('[data-linkType*=modal-trigger]'), function(i,v){
                util.modalize($(v));
            });
            //allows class based CSS animation to happen anywhere
            $.each($('[data-action="screengo"]'), function(i,v){
                util.screengo($(v));
            });

		}
	};

});
