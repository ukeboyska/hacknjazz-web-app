/*global Modernizr*/

require(['jquery','underscore','zugzug','headroom'/*,'util'*/], function ($, _, zugzug, Headroom/*,util*/) {

    'use strict';
    
    zugzug.app.start();

    // grab an element
    var myElement = document.querySelector('body');
    // construct an instance of Headroom, passing the element
    var headroom  = new Headroom(myElement);
    // initialise
    headroom.init(); 

    //adjusts height of all columns inside wrapping element to be the same
    /*$.each($('.heightify'), function(i,v){
		util.heightify($(v));
    });*/

    //allows class based CSS animation to happen anywhere
    /*$.each($('[data-action="screengo"]'), function(i,v){
		util.screengo($(v));
    });*/

    /*$.each($('[data-linkType*=modal-trigger]'), function(i,v){
        util.modalize($(v));
    });*/

	/*$.each($('*[data-target]'), function(){
        $(this).on('click touchstart', function(event){
            event.preventDefault();
            util.scrollTo($(this));
        });
    });*/

    $('body').addClass('no-touching');

    //touch specifics!
    if (window.navigator.msMaxTouchPoints || Modernizr.touch){
        
        /*var clickHandler = function(e) {
            e.preventDefault();
        };*/
        
        $('body').removeClass('no-touching').addClass('touching');
        $('body').on('touchstart', function(){
            /*$('[data-atom="main-navigation--pagelinks"] .tier-one > li').removeClass('touchable');*/
        });

        /*$('[data-atom="main-navigation--pagelinks"] ul').addClass('touch');
        $('[data-atom="main-navigation--pagelinks"] .tier-one > li > a, [data-atom="main-navigation--pagelinks"] .tier-one > li > h2 > a').on('touchstart', clickHandler);
        $('[data-atom="main-navigation--pagelinks"] .tier-one > li').removeClass('hoverable').on('touchstart',function(e){
            e.stopPropagation();
            $('[data-atom="main-navigation--pagelinks"] .tier-one > li').removeClass('touchable').find('a').on('touchstart', clickHandler);
            $(this).addClass('touchable').find('a').off('touchstart', clickHandler);
        });

        $('[data-atom="main-navigation--pagelinks"] .tier-two').on('touchstart',function(e){
            e.stopPropagation();
            $(this).closest('[data-atom="main-navigation--pagelinks"]').addClass('touchable');
        });*/

    }

});