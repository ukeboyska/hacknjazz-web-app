# hacK'n Jazz Web App #

By Yurij Roman Lojko, Lojko & Co. LLC

Dear wonderful front end coder,

The documentation below should help you jump on this project easily and smoothly. If you read the following and make a concerted effort to continue the best practices defined below, then there will be a greater balance in the Universe.

“As a skeptic, I would ask for consistency first of all.” 
― Sylvia Plath

- - - -

## Installation ##

All prerequisite requirements found [here](https://docs.google.com/document/d/14Ih2ieaGyhfZ6-fZ46yY6YQNe2tvhLagj63XHfGp-Ds/edit?usp=sharing)

After installing or updating those on your computer, open your console and run:

```console
npm install
```
then run:

```console
bower install
```
after that,

```console
gulp serve
```
...should open a new livereload tab in your browser. Gulp is faster than Grunt but can be a bit finicky. You might have to double save a file or reload the browser for the changes to show up (particularly on the main index.html page). When you're ready to build, run:

```console
gulp
```
...viola! You now have a project you can confidently push live. Note: the production files will only work if they're the root of the site. You will have to edit the gulpfile.js a bit to make the production files work in sub folders.

- - - -

## The Index ##

The index file inside app/ contains a list of all this site's templates. Whenever a new template is added, be sure to add a link to it in the index file, as well as in the gulpfile.js:

```javascript
gulp.task('fileinclude', () => {
  return gulp.src('app/**/*.html')
    .pipe($.plumber())
    .pipe($.fileInclude({ 
      prefix: '@@', 
      basepath: '@file', 
      context: { 
        template: 'index', 
        template: 'generic', 
        //add any new templates here
        template: 'homepage'
      } 
    }))
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({stream: true}));
});
```
- - - -

## Naming Conventions ##

Please take a moment to study the general naming convention of the site, applied to any stylesheet, any image, any script. And everthing connects to everything else. The idea is underscore [__] to separate category/type from thing, double dash for any sub thing/category/type [--], dash for any space [-]. 

For example: 

	inner__about--clinical-services
	inner__about--clinical-services--landing
	...

* don't use capital letters
* please keep it going, at least for this project
	* do it for the children (will somebody *please* think of the children?)

- - - -

## Javascript Notes ##

I'm using require.js and Carlos Escobar's zugzug library to organize the JS and fire different code for different modules. By organizing code into modules, it should make it easy for any serious JS developer to upgrade to Backbone,js, Ember.js, Angular.js, or any other MVC framework.

A module is any group of functions that affect an element. To create new modules, simply create new folders inside the modules folder and follow the nomenclature there, then call them by putting the appropriate data-module attribute on the appropriate element. Check out https://bitbucket.org/ukeboyska/zugzug for more information.

The app.main.js file is for JS that can potentially be used on any template.

util.js has all common / pluginized functions that don't necessarily apply to any specific module.

The iDangero.us Swiper is being used for all swiping elements. Before creating a new swiper, take a look and see how the already existing swipers are functioning and see if you can reuse the main type of Swipers defined in modules/swiper before creating your own.

* further iDangero.us Swiper documentation: <http://idangero.us/swiper/>

Youtube (or vimeo) embeds are used for videos.

There's an existing way to create modals (check util!). Right now it's just used for videos -- feel free to extend for other purposes.

- - - -

## Stylesheets and Their Purposes ##

* 0__utility is the center of it all

	* _0__fonts is where the "fontello" gulp task automatically puts font icon files
		* the task runs automatically when you're coding. just drop in a new config.json file you get from <http://fontello.com> and viola! 
	
	* _1__options.scss defines lots of variables and site constants
		* open it and look it over before doing anything else.
	
	* _2__fonts.scss includes any custom embedded font
		* put the correspending font files inside unique folders inside /fonts
	
	* _3__base.scss includes a lot of shared styles across the whole site
		* before you create a new style, make sure it's not already created here

* 1__regions 

	* _global-body.scss includes a lot of overarching styles that are used in the "main meat" of the site
		* by "main meat," I mean parts of the site that are not the footer, not the header...you dig?
		* use it for any new typography, etc.
		* note: it only applied to parts of the site with a wrapping "global-body" data-region tag

	* _global-header.scss and _global-footer.scss are obvious other regions

* 2__pieces
	* really whatever isn't a region or template is a piece. feel free to split this up into sub-pieces if you need to.

* 3__templates
	* for new templates (with un-shared styles!) created in the site.
	* by templates, I mean one of the .php files in the templates folder
	* follow the naming convention that already exists
	* if you create a new template folder/stylesheet, attach a data attribute on a wrapping element in the template that follows this syntax: 
		`data-template="template-name"`
	* note: sometimes you'll have pieces inside of templates that you defined somewhere else, but have special styles only on those templates. in that case, keep the *original names* you created and feel free to put those altered styles within the template stylesheet.
		* the main point is: figure out what the main element is and how it's used throughout the site.
		* base inheritance off of an elements' purpose, use it, then extend it

* 4__vendor includes styles for plugins that aren't in bower_components

* 5__misc includes miscellaneous styles that didn't fit anywhere else
	* _ie.scss - includes all ie hacks
		* keep hacks minimal and all within that 1 sheet
	* _touching.scss is for touch device specifics (mainly links, navigations, clicks)
	* ...

- - - -

##	Atomic Web Stylesheets/elements/approach ##

Before you create any new element/area/whatever, make sure it's not already created anywhere else.

This site uses data-attributes to name different kinds of elements throughout the site for styling and organizational purposes. It was inspired by [Atomic Web Design](http://bradfrostweb.com/blog/post/atomic-web-design/) terms to form those data attributes. Confused? Hopefully these examples below will clarify:

* when you create new regions, follow this naming convention: 
	`data-region="region-name"`
* when you create new pieces, follow this naming convention: 
	`data-piece="piece-type__piece-name--more-specific" -OR- data-piece="piece-name"`
* when you create new templates, follow this naming convention: 
	`data-template="template-name"`

- - - -

## Misc. Styling Conventions, Tools, & Approaches ##

* fonts
	* use font variables defined in options.
	* DO NOT use font weights on elements unless you are using traditional fonts with all weights automatically installed
	* only put font sizes on individual elements, like <li>, <p>, <a>, <span> and use the EM mixin
		* example: if you want a <p> element to be 20px big, 
		`p {
			font-size: em(20);
		}`
	* never put a font size on a wrapping element...
		* EXCEPTION: sometimes <a> tags are within <p> tags, right? if you put a size on the wrapping <p>, wouldn't it affect the a inside of it too?
			* nope. should be fine since I have inherits in those cases, and they should take care of those issues.	

* grid system: use grid (griddle) for any columns/layouts
	* example:
		`<element class="grid">
			<element class="grid__cell unit-6-12">
				content
			</element>
			<element class="grid__cell unit-3-12">
				content
			</element>
			<element class="grid__cell unit-3-12">
				content
			</element>
		</element>`
		* note how 6 + 3 + 3 = 12, you dig?
		* if you want to make it 100% on mobile, just add class 'mobile__100' to the grid cell
			* example:
				`<element class="grid__cell unit-6-12 mobile__100">`
			* also available: tablet__20, tablet__25, tablet__50, tablet__100, mobile__25, mobile__50, mobile__100, tiny__25, tiny__50, tiny__100 
				* need more? make your own!
		* this site uses 12 column grids systems. try to keep it to those two.
			* ...but if you need to add another one, do it in _options.scss
		* further documentation: <http://necolas.github.io/griddle/>

* general layout 
	* widths: use percentages
	* padding and margins (left + right) : use percentages

* use symantic HTML5 elements
	* before you create any element, ask yourself: "Is there an HTML5 element that might make this element more semantic?"
	* if you're not sure which element to use, consult this site:
		* <https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5/HTML5_element_list>
	* check to make sure your HTML5 elements are used/titled correctly with this tool: 
		* <https://chrome.google.com/webstore/detail/html5-outliner/afoibpobokebhgfnknfndkgemglggomo>
		* if you have any "UNTITLED SECTIONS" listed, remember that any <nav>,<section>,<article> element requires a header tag within it.
			* if there's no header to visually display, still put one in and give it class="screenreader" and make sure the header refers logically to the content inside the element
				* for example
					`<h1 class="screenreader">hacK'n Jazz Homepage Spotlight</h1>`
		* use @extend if you have a class that's reused in multiple elements
			* generally put extended classes inside the _3__base.scss file...or else have a good reason not to
		* no need to use mixins for CSS3 - using a post processor to figure out fallbacks for old browsers
		* background colors: if you use rgba, have a hex fallback above it
		* always follow my indentation pattern (keep my code pretty)!

- - - -

## Articles That Helped Inspire Methodologies Used In This App##

* http://bradfrostweb.com/blog/post/atomic-web-design/
* https://github.com/north/north
* http://patternlab.io/
* https://medium.com/objects-in-space/f6f404727
* http://operatino.github.io/MCSS/en/
* http://ejohn.org/blog/html-5-data-attributes/
* https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_data_attributes
* http://drewbarontini.com/articles/single-responsibility

I was also inspired by many people I worked with for 6+ years over at [Genuine Interactive](http://wearegenuine.com)

Thanks, <3, have fun!

--Yurij


- - - -
- - - -
- - - -


## Post Script ##

“Having, then, once introduced an element of inconsistency into his system, he was far too consistent not to be inconsistent consistently, and he lapsed ere long into an amiable indifferentism which to outward appearance differed but little from the indifferentism …” 
― Samuel Butler

“Consistency is good, but progress is better.” 
― Amit Kalantri

“Basically, I'm really impressionable and have no sense of consistency in anything I do.” 
― [Grimes](https://www.youtube.com/watch?v=JtH68PJIQLE)