// generated on 2017-01-20 using generator-webapp 2.3.2
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');
const fileinclude = require('gulp-file-include');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var dev = true,
  sassGlob = require('gulp-sass-glob'),
  fontello = require('gulp-fontello'),
  rename = require('gulp-rename'),
  requirejsOptimize = require('gulp-requirejs-optimize');


gulp.task('fontello', function () {
  currentTask = 'fontello';
  return gulp.src('app/fonts/icons/config.json')
  .pipe($.plumber())
  .pipe($.fontello({
    font: 'fonts/icons',
    css: 'styles/0__utility/_0__icons'
  }))
  .pipe($.rename(function(file) { // we need to rename css files to scss
    if (file.extname == '.css') {
      file.extname = '.scss';
    }
  }))
  .pipe(gulp.dest('app/'))
  .pipe(reload({stream: true}));
});

gulp.task('fileinclude', () => {
  return gulp.src('app/**/*.html')
    .pipe($.plumber())
    .pipe($.fileInclude({ 
      prefix: '@@', 
      basepath: '@file', 
      context: { 
        template: 'index', 
        template: 'generic', 
        //add any new templates here
        template: 'homepage'
      } 
    }))
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({stream: true}));
});

gulp.task('styles', () => {
  return gulp.src('app/styles/*.scss')
    .pipe(sassGlob())
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()))
    .pipe(reload({stream: true, once: true}));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js')
    .pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['styles', 'scripts'], (cb) => {
  return gulp.src('app/**/*.html')
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if('*.html', $.fileInclude({ 
      prefix: '@@', 
      basepath: '@file', 
      context: { 
        template: 'index', 
        template: 'generic', 
        //add any new templates here
        template: 'homepage'
      } 
    })))
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe(gulp.dest('dist'));
    cb(err);
});

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('app/fonts/**/*'))
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*',
    '!app/**/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});


gulp.task('requirejsBuild', function() {
    return gulp.src('app/scripts/modules/**/*.js')
    .pipe(requirejsOptimize(function(file) {
      return {
        name: '../../node_modules/almond/almond',
        mainConfigFile: './app/scripts/require.config.js',
        optimize: 'uglify',
        useStrict: true,
        out: 'scripts/app.built.js',
        baseUrl: 'app/scripts',
        include: [
          'app.main'
        ],
      };
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));
gulp.task('cleanDist', del.bind(null, ['dist/+includes','dist/+templates']));

gulp.task('serve', () => {
  runSequence(['clean', 'wiredep'], ['fileinclude', 'styles', 'scripts', 'fonts'], () => {
    browserSync.init({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });
    gulp.watch('app/**/*.html', ['fileinclude']);
    gulp.watch('app/fonts/**/*.json', ['fontello']);
    gulp.watch('app/styles/**/*.scss', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('app/fonts/**/*', ['fonts']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);

    gulp.watch([
      'app/*.html',
      'app/images/**/*',
      '.tmp/fonts/**/*'
    ]).on('change', reload);
  });
});

gulp.task('serve:dist', ['default'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('app/styles/*.scss')
    .pipe($.filter(file => file.stat && file.stat.size))
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});
 
gulp.task('build', ['lint', 'fontello', 'html', 'images', 'fonts', 'extras'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  return new Promise(resolve => {
    dev = false;
    runSequence(['clean', 'wiredep'], 'build', 'requirejsBuild', 'cleanDist', resolve);
  });
});


